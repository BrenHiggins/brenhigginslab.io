---
permalink: /about/
title: "About"
classes: wide
---
Brendan Higgins is a Cybersecurity Engineer.

This site is just a place to record interesting things in cybersecurity, technology, programming, literature, video games, and just about anything else.

# Education:
* Western Governors University: Master of Science, Cyber Security and Information Assurance
* California State University San Bernardino: Bachelor's Degree, Business Administration, Cyber Security

# Certifications:
* (ISC)² - CISSP
* Amazon Web Services (AWS) - Cloud Certified Practitioner
* GitLab DevOps Professional
<hr>

* GIAC Cloud Security Automation (GCSA)
* GIAC Exploit Researcher and Advanced Penetration Tester (GXPN)
* GIAC Certified Windows Security Administrator (GCWN)
* GIAC Certified Forensic Analyst (GCFA)
* GIAC Continuous Monitoring Certification (GMON)<sup style="font-size:.75em;">Expired<sup>
* GIAC Certified Intrusion Analyst (GCIA)<sup style="font-size:.75em;">Expired<sup>

<hr>

* CompTIA - Advanced Security Practitioner (CASP)
* CompTIA - Security+
* CompTIA - Cloud+
* CompTIA - Network+
* CompTIA - PenTest+

<i>These also combine into all sorts of neat <a href="https://www.comptia.org/certifications/which-certification/stackable-certifications" target="_blank">stackable</a> certification titles.</i>

<hr>

* EC-Council - Computer Hacking Forensic Investigator (CHFI) <sup style="font-size:.75em;">Expired<sup>
* EC-Council - Certified Ethical Hacker (CEH) <sup style="font-size:.75em;">Expired<sup>

<hr>

* Federal Communications Commission - General Class HAM Radio License