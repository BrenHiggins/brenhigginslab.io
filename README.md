# Build
Example [Jekyll] website using GitLab Pages.  View it live at https://pages.gitlab.io/jekyll

[Learn more about GitLab Pages](https://pages.gitlab.io) or read the the [official GitLab Pages documentation](https://docs.gitlab.com/ce/user/project/pages/).

## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Clone this project
1. Install Jekyll
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`

* The [jekyll-themes](https://gitlab.com/groups/jekyll-themes) group contains a collection of example projects you can fork (like this one) having different visual styles.

# Notes
https://gitlab.com/pages/jekyll

